<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Repository\VideoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VideoRepository::class)]
class Video
{
    use UuidTrait;


    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $owner;

    #[ORM\Column(type: 'string', length: 5000)]
    private string $awsKey;

    #[ORM\Column(type: 'string', length: 5000)]
    private string $url;

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getAwsKey(): string
    {
        return $this->awsKey;
    }

    public function setAwsKey(string $awsKey): self
    {
        $this->awsKey = $awsKey;
        return $this;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
        ];
    }
}
