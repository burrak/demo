<?php

declare(strict_types = 1);

namespace App\Utility;

class Pagination
{
    public function __construct(private int $page = 1, private ?int $pageSize = null)
    {
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }
}
