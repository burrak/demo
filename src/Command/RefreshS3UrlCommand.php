<?php

declare(strict_types = 1);

namespace App\Command;

use App\Repository\ImageRepository;
use App\Repository\VideoRepository;
use Aws\CommandInterface;
use Aws\S3\S3Client;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 's3:url:update',
    description: 'Update S3 presigned URLs.',
    hidden: false
)]
class RefreshS3UrlCommand extends Command
{
    public function __construct(private ImageRepository $imageRepository, private VideoRepository $videoRepository, private S3Client $s3Client, private string $bucket, string $name)
    {
        parent::__construct($name);
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        /** @var \App\Entity\Image[] $images */
        $images = $this->imageRepository->findAll();
        foreach ($images as $image) {
            $cmd = $this->getCmd($image->getAwsKey());
            $request = $this->s3Client->createPresignedRequest($cmd, '+24 hours');
            $presignedUrl = (string)$request->getUri();

            $image->setUrl($presignedUrl);
            $this->imageRepository->save($image, true);
        }

        /** @var \App\Entity\Video[] $videos */
        $videos = $this->videoRepository->findAll();
        foreach ($videos as $video) {
            $cmd = $this->getCmd($video->getAwsKey());
            $request = $this->s3Client->createPresignedRequest($cmd, '+24 hours');
            $presignedUrl = (string)$request->getUri();

            $video->setUrl($presignedUrl);
            $this->videoRepository->save($video, true);
        }

        return 0;
    }

    private function getCmd(string $key): CommandInterface
    {
        $cmd = $this->s3Client->getCommand('GetObject', [
            'Bucket' => $this->bucket,
            'Key' => $key,
        ]);

        return $cmd;
    }

}
