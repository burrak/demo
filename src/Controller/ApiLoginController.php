<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class ApiLoginController
{
    public function __construct(private JWTTokenManagerInterface $JWTTokenManager)
    {
    }

    #[Route('/api/login', name: 'api_login')]
    public function index(#[CurrentUser] ?User $user): Response
    {
        if ($user === null) {
            return new JsonResponse([
                'message' => 'missing credentials',
            ], Response::HTTP_UNAUTHORIZED);
         }

        $token = $this->JWTTokenManager->create($user);

        return new JsonResponse([
            'user'  => $user->getUserIdentifier(),
            'token' => $token,
        ]);
    }
}
