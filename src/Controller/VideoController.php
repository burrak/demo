<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class VideoController
{
    public function __construct(private FileUploader $fileUploader)
    {
    }

    #[Route('/api/video', methods: Request::METHOD_POST)]
    public function upload(Request $request): Response
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        // upload the file and save its filename
        $this->fileUploader->upload($uploadedFile);

        return new JsonResponse();

    }
}
