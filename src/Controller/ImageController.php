<?php

declare(strict_types = 1);

namespace App\Controller;

use App\DTO\ImageInputDTO;
use App\Service\ImageService;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

class ImageController
{
    public function __construct(
        private ImageService $albumService,
        private Security $security
    )
    {
    }

    #[Route('/api/image', methods: Request::METHOD_POST)]
    public function post(#[MapRequestPayload] ImageInputDTO $album): Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->security->getUser();
        $album = $this->albumService->createImages($album, $user);

        return new JsonResponse($album);
    }
}
