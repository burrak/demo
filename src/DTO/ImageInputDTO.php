<?php

declare(strict_types = 1);

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ImageInputDTO
{
    public function __construct(
        #[Assert\Count(min: 1)]
        private readonly array $images
    )
    {
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }
}
