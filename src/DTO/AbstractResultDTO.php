<?php

declare(strict_types = 1);

namespace App\DTO;

use JsonSerializable;

/**
 * @template T
 */
abstract class AbstractResultDTO implements JsonSerializable
{
    /**
     * @param array<T> $data
     * @param int $items
     * @param int $page
     * @param int $pageSize
     */
    public function __construct(
        private array $data,
        private int $items,
        private int $page,
        private int $pageSize
    )
    {
    }

    public function jsonSerialize(): mixed
    {
        return [
            'items' => $this->items,
            'page' => $this->page,
            'pageSize' => $this->pageSize,
            'data' => $this->data,
        ];
    }
}
