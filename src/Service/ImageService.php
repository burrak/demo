<?php

declare(strict_types = 1);

namespace App\Service;

use App\DTO\ImageInputDTO;
use App\Entity\Image;
use App\Entity\User;
use App\Repository\ImageRepository;
use App\Utility\UploadedBase64File;
use Aws\S3\S3Client;

class ImageService
{
    public function __construct(private FileUploader $fileUploader, private ImageRepository $imageRepository)
    {
    }


    /**
     * @return string[]
     */
    public function createImages(ImageInputDTO $imageInputDTO, User $owner): array
    {
        $imageUrls = [];
        foreach ($imageInputDTO->getImages() as $imageDTO) {
            $image = new Image();
            $image->setOwner($owner);
            $mimeType = mime_content_type($imageDTO);
            if ($mimeType === false) {
                throw new \InvalidArgumentException('Invalid content type');
            }
            $extension = explode('/', $mimeType)[1];
            $fileName = uniqid() . '.' . $extension;
            $dataArray = explode(',', $imageDTO);
            $file = new UploadedBase64File($dataArray[1], $mimeType, $fileName, $extension);
            $key = $this->fileUploader->upload($file);


            $s3 = new S3Client([
                'region'  => 'eu-central-1',
                'version' => 'latest',
                'credentials' => [
                    'key'    => "AKIAUG6LSUWMUZOBVEFS",
                    'secret' => "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",
                ],
            ]);

            $cmd = $s3->getCommand('GetObject', [
                'Bucket' => 'bucket-viedos',
                'Key' => $key,
            ]);
            $request = $s3->createPresignedRequest($cmd, '+24 hours');
            $presignedUrl = (string)$request->getUri();

            $image->setUrl($presignedUrl);
            $image->setAwsKey($key);
            $this->imageRepository->save($image, true);
        }

        return $imageUrls;
    }
}
