<?php

declare(strict_types = 1);

namespace App\MessageHandler;

use App\Message\ImageMessage;
use App\Repository\ImageRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class ImageMessageHandler
{
    public function __construct(
        private string $imagesDir,
        private ImageRepository $imageRepository
    )
    {
    }

    public function __invoke(ImageMessage $message): void
    {
        // TODO: S3 upload
        /** @var \App\Entity\Image $image */
        $image = $this->imageRepository->find($message->getImageId());
        unlink($this->imagesDir . $image->getUrl());
        $image->setUrl('https://s3.com' . $image->getUrl());
        $this->imageRepository->save($image, true);
    }
}
