<?php

declare(strict_types = 1);

namespace App\Message;

use Ramsey\Uuid\UuidInterface;

class ImageMessage
{
    public function __construct(
        private UuidInterface $imageId
    )
    {
    }

    public function getImageId(): UuidInterface
    {
        return $this->imageId;
    }
}
