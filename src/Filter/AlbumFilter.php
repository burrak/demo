<?php

declare(strict_types = 1);

namespace App\Filter;

class AlbumFilter
{
    public function __construct(private ?string $ownerId = null)
    {
    }

    public function getOwnerId(): ?string
    {
        return $this->ownerId;
    }
}
