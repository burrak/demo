<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Persistence\ManagerRegistry;

class VideoRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Video::class);
    }
}
