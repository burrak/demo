<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Persistence\ManagerRegistry;

class ImageRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }
}
