<?php

declare(strict_types = 1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @template T
 */
abstract class AbstractRepository extends ServiceEntityRepository
{
    /**
     * @param T $entity
     *
     * @return T
     */
    public function save($entity, bool $flush = false)
    {
        $this->_em->persist($entity);

        if (!$flush) {
            return $entity;
        }
        $this->_em->flush();

        return $entity;
    }

    /**
     * @param T $entity
     */
    public function remove($entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if (!$flush) {
            return;
        }
        $this->getEntityManager()->flush();
    }

}
