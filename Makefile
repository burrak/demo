install:
	docker-compose up -d
	docker-compose exec php composer install
	docker-compose exec php php bin/console lexik:jwt:generate-keypair
	docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction
	docker-compose exec php bin/console doctrine:fixtures:load --no-interaction

phpcs:
	docker-compose exec php ./vendor/bin/phpcs -p --standard=./tests/php_codesniffer/ruleset.xml src

phpstan:
	docker-compose exec php ./vendor/bin/phpstan analyse -c phpstan.neon src

phpunit:
	docker-compose exec php ./vendor/bin/phpunit

phpcopy:
	docker-compose exec php rm -rf ../php_cpd
	docker-compose exec php composer create-project sebastian/phpcpd ../php_cpd
	docker-compose exec php ../php_cpd/phpcpd src
	docker-compose exec php rm -rf ../php_cpd
